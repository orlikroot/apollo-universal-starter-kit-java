//package graphql.global.utils;
//
//import io.github.cdimascio.dotenv.Dotenv;
//import org.springframework.stereotype.Component;
//
//@Component
//public class EnvironmentVariables {
//
//    private static Dotenv dotenv;
//
//    public EnvironmentVariables() {
//        dotenv = Dotenv.load();
//    }
//
//    public static String getDotEnvVariable(String value) {
//        return dotenv.get(value);
//    }
//}
