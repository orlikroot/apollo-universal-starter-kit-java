package graphql.schema;

public interface OperationNames {
    String ADD_USER = "addUser";
    String EDIT_USER = "editUser";
    String DELETE_USER = "deleteUser";
}
